﻿Shader "Unlit/Phong"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Diffuse ("Diffuse Color", Color) = (1,1,1,1)
		_AmbientLightColor ("Ambient Light Color", Color) = (1,1,1,1)
		_AmbientFactor ("Ambient Light Strength", Range(0,2)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "LightMode"="ForwardBase" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1;
				float3 worldNormal : TEXCOORD2;
				fixed4 finalColor : TEXCOORD3;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float4 _AmbientLightColor;
			float4 _Diffuse;
			float _AmbientFactor;

            v2f vert (appdata_base v)
            {
                v2f o;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.worldNormal = normalize(mul(v.normal, (float3x3)unity_WorldToObject));
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);

				float3 lightDir = normalize(_WorldSpaceLightPos0.xyz);
				float4 NDotL = saturate(dot(o.worldNormal, lightDir));
				o.finalColor = _Diffuse * NDotL + _AmbientLightColor * _AmbientFactor;
                return o;
            }

			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 finalCol = i.finalColor;
                return finalCol;
            }
            ENDCG
        }
    }
}
